import calculadora

operacoes = {
    '1': 'Adição',    
    '2': 'Subtrair',    
    '3': 'Multiplicar',    
    '4': 'Dividir',    
    '5': 'Potencia',   
    '6': 'Sair'   
}

fnc_dict = {
    '1': calculadora.add,
    '2': calculadora.sub,
    '3': calculadora.mult,
    '4': calculadora.div,
    '5': calculadora.pot,
}

while True:
    print('## Escolha uma opção')
    for i, op in operacoes.items():
        print (f'{i}. {op}')
    
    escolha = input('Digite o número da opção desejada ')    
    
    if escolha not in operacoes.keys():
        print('Opção inválida')
        continue

    rs = operacoes.get(escolha)
    if rs == 'Sair':
        print('Saindo')
        break
    
    print(f'{rs} escolhida')

    fst = int(input('primeira entrada: '))
    snd = int(input('secunda entrada: '))

    p = fnc_dict.get(escolha)(fst, snd)

    print(f'Resultado: {p}')
    